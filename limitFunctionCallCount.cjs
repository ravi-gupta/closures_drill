function limitFunctionCallCount(callBack, numberOfTimes) {
    if (typeof numberOfTimes !== "number") {
        return function () {
            console.log("Number of times is not a valid time");
            return null;
        };
    }
    let counter = 0;

    return function (someArgument) {
        if (counter < numberOfTimes) {
            counter++;
            if (typeof someArgument !== "number") {
                console.log(
                    `Input argument is not a number, But your Counter Increases to ${counter}, you have ${numberOfTimes - counter
                    } chance`
                );
            } else {
                console.log(
                    `Counter become ${counter}, you perform execution some logic on ${someArgument} variable, you have ${numberOfTimes - counter
                    } chances left`
                );
                return callBack(someArgument);
            }
        }
        if (counter === numberOfTimes) {
            console.log(
                `you not able to execute logic anymore, Please Increase Counter and try Again!!`
            );
            return null;
        }
    };
}
module.exports = limitFunctionCallCount;
