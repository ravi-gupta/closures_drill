function counterFactory(){
    let counter = 0;
    return{
        increment: function increment(){
            return ++counter;
        },
        decrement: function decrement(){
            return --counter;
        },
        getCounter: function getCounter(){
            return counter;
        }
    }
}
module.exports = counterFactory;