const cacheFunction = require("../cacheFunction.cjs");

let counter = 0;
const tempCacheFunction = cacheFunction((element) => {
  counter++;
  if (typeof element === "number") {
    return element * 2;
  } else {
    console.log("Your Argument is not a number, So No logic Applied!!");
    return element;
  }
});
const obj = {
  name: "Ravi",
  lastname: "Gupta",
  age: 22,
};

console.log(tempCacheFunction(obj));
console.log(`You Call Counter is ${counter}`);

console.log(tempCacheFunction(10));
console.log(`You Call Counter is ${counter}`);

console.log(tempCacheFunction(20));
console.log(`You Call Counter is ${counter}`);

console.log(tempCacheFunction(50, 'hello'));
console.log(`You Call Counter is ${counter}`);

console.log(tempCacheFunction("hello"));
console.log(`You Call Counter is ${counter}`);
