const counterFactory = require('../counterFactory.cjs');

const counterVariable = counterFactory();

console.log(`Initial value of counter is ${counterVariable.getCounter()}`);

console.log(counterVariable.increment());
console.log(counterVariable.decrement());
console.log(counterVariable.decrement());
console.log(counterVariable.increment());
console.log(counterVariable.increment());

console.log(`Final value of counter is ${counterVariable.getCounter()}`);