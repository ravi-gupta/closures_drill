const limitFunctionCallCount = require("../limitFunctionCallCount.cjs");

const count = 3;
const limitFun = limitFunctionCallCount((element) => {
    return element * 2;
}, count);

const limitFirstTime = limitFun(2);

console.log(limitFirstTime);

const limitSecondTime = limitFun(10);

console.log(limitSecondTime);

const limitThirdTime = limitFun(15);

console.log(limitThirdTime);

const limitFourthTime = limitFun(20);

console.log(limitFourthTime);  