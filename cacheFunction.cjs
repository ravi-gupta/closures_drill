function cacheFunction(callBack) {
  if (typeof callBack !== "function") {
    return function () {
      console.log("Callback function is Incorrect!!");
      return null;
    };
  }
  const cache = {};
  return function (...someArgument) {
    const key = JSON.stringify(someArgument);

    if (cache[key] === undefined) {
      cache[key] = callBack(...someArgument);
    } else {
      console.log("Key is already there, you have enter unique value!!");
      return cache;
    }
    console.log(`Current Cache memory is:`);
    console.log(cache);
    return cache[key];
  };
}
module.exports = cacheFunction;
